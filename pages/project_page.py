from selenium.common import TimeoutException
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from fixtures.testarena.login import browser
from pages.add_project_page import AddProjectPage
from fixtures.testarena.data_string import generate_random_string


class ProjectPage:
    project_page_add_new_button = (By.CSS_SELECTOR, '.button_link_li:first-child')
    project_page_button = (By.CSS_SELECTOR, '.icon_tools')

    def __init__(self, browser):
        self.browser = browser
        self.wait = WebDriverWait(browser, 10)

    def go_to_add_project_page(self):
        self.wait.until(EC.presence_of_element_located(self.project_page_add_new_button))
        self.browser.find_element(*self.project_page_add_new_button).click()
        return AddProjectPage(self.browser)

    def find_element_on_home_page(self):
        return self.browser.find_element(*self.project_page_add_new_button).text

    # def is_loaded(self, wait_for_element, timeout=10):
    #     return wait_for_element(self.browser,self.project_page_add_new_button,timeout)

    def is_loaded(self):
        try:
            WebDriverWait(self.browser, 10).until(
                EC.presence_of_element_located(self.project_page_add_new_button)
            )
            return True
        except:
            return False
