import pytest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

@pytest.fixture
def browser():
    driver = webdriver.Chrome()  # Użyj odpowiedniego drivera dla swojej przeglądarki
    yield driver
    driver.quit()

def test_element_exists(browser):
    browser.get('http://example.com')  # URL strony do testowania
    try:
        element = WebDriverWait(browser, 10).until(
            EC.presence_of_element_located((By.CSS_SELECTOR, ".your-css-selector"))
        )
        assert element.is_displayed(), "Element is present and displayed."
    except:
        pytest.fail("Element not found within the given time.")